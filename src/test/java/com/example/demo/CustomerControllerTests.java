package com.example.demo;

import org.mockito.BDDMockito;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.example.demo.controller.CustomerController;
import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.ICustomerService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ICustomerService service;

    @MockBean
    private CustomerRepository repository;

    @Before
    public void setUp() throws Exception {}

    @Test()
    public void givenCustomers_whenGetCustomers_thenReturnJsonArray() throws Exception {

        Customer customer1 = new Customer(0L, "1");
        Customer customer2 = new Customer(0L, "2");
        Customer customer3 = new Customer(0L, "3");
        List<Customer> customerList = Arrays.asList(customer1, customer2, customer3);

        BDDMockito.given(service.findAll(Optional.ofNullable(null))).willReturn(customerList);

        mvc.perform(get("/customers").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        verify(service, VerificationModeFactory.times(1)).findAll(Optional.ofNullable(null));
        reset(service);
    }
}
