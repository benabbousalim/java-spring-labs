package com.example.demo;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import com.google.gson.Gson;

import com.example.demo.controller.CustomerController;
import com.example.demo.controller.handlers.RestExceptionHandler;
import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
public class ITCustomerControllerTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private CustomerController customerController;

    @Autowired
    private CustomerRepository repository;

    private final Gson gson = new Gson();

    @Before
    public void setUp() throws Exception {
        repository.deleteAll();
        mvc = MockMvcBuilders.standaloneSetup(customerController).setControllerAdvice(new RestExceptionHandler()).build();

    }

    @After
    public void clean() throws Exception {
        repository.deleteAll();

    }

    @Test()
    public void whenGetCustomers_thenReturnEmptyJsonArray() throws Exception {

        // Customer customer1 = new Customer(0L, "1");
        // Customer customer2 = new Customer(0L, "2");
        // Customer customer3 = new Customer(0L, "3");
        // List<Customer> customerList = Arrays.asList(customer1, customer2, customer3);

        // BDDMockito.given(service.findAll(Optional.ofNullable(null))).willReturn(customerList);

        mvc.perform(get("/customers?page=0&size=2").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0))).andReturn();
    }

    @Test()
    public void whenGetCustomers_thenReturnJsonArray() throws Exception {
        Customer customer1 = new Customer(0L, "1");
        Customer customer2 = new Customer(0L, "2");
        Customer customer3 = new Customer(0L, "3");
        List<Customer> customerList = Arrays.asList(customer1, customer2, customer3);

        repository.saveAll(customerList);

        mvc.perform(get("/customers?page=0&size=3").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3))).andReturn();

    }

    @Test
    public void whenGetCustomersById_thenReturnOnElement() throws Exception {
        Customer customer1 = new Customer(0L, "1");

        customer1 = repository.save(customer1);

        MvcResult result = mvc.perform(post("/customers/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        Customer response = (Customer) gson.fromJson(result.getResponse().getContentAsString(), Customer.class);
        assertEquals(customer1, response);

    }

}
