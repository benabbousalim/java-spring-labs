package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hello")
public class HelloController {

    @GetMapping
    ResponseEntity<String> hello() {
        ResponseEntity<String> responseEntity = new ResponseEntity<String>("Hello  toto", HttpStatus.OK);
        return responseEntity;
    }
}
