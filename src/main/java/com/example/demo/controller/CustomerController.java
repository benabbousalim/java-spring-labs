package com.example.demo.controller;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import com.example.demo.controller.exceptions.NotFoundException;
import com.example.demo.model.Customer;
import com.example.demo.service.ICustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("customers")
public class CustomerController {

    @Autowired
    private ICustomerService customerService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer save(@RequestBody() Customer customer) {
        return this.customerService.save(customer);
    }

    @GetMapping
    public Collection<Customer> getAll(@RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {

        Optional<Pageable> pageOpt = page != null && size != null ? Optional.of(PageRequest.of(page, size))
                : Optional.empty();
        return this.customerService.findAll(pageOpt).stream().filter(c -> true).collect(Collectors.toList());
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Customer> getCustomerById(@PathVariable("id") int id) throws NotFoundException {

        try {
            Customer cust = this.customerService.findById((long) id);
            return new ResponseEntity<Customer>(cust, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            throw  new NotFoundException("NOT.FOUND", "not found exception");
        }
    }

    @PutMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Customer> updateCustomer(@PathVariable("id") int id, @RequestBody() Customer customer) {
        if (this.customerService.exists((long) id)) {
            Customer updatedCustomer = this.customerService.save(customer);
            return new ResponseEntity<Customer>(updatedCustomer, HttpStatus.OK);
        }

        return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<Boolean> deleteCustomer(@PathVariable("id") int id) {
        if (this.customerService.exists((long) id)) {
            this.customerService.delete((long) id);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }

        return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
    }

}
