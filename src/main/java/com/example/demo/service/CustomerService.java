package com.example.demo.service;

import java.util.Collection;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CustomerService implements ICustomerService {

    @Autowired()
    private CustomerRepository customerRepository;

    public Collection<Customer> findAll(Optional<Pageable> pageable) {
        Page<Customer> page = this.customerRepository.findAll(pageable.get());
        return page.getContent();
    }

    public Customer findById(Long id) throws EntityNotFoundException {
        return this.customerRepository.getOne(id);
    }

    public boolean exists(Long id) throws EntityNotFoundException {
        return this.customerRepository.existsById(id);
    }

    public boolean delete(Long id) throws EntityNotFoundException {
        this.customerRepository.deleteById(id);
        return true;
    }

    public Customer save(Customer customer) {
        return this.customerRepository.save(customer);
    }

}
