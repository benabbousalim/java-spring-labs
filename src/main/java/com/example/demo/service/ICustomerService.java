package com.example.demo.service;

import java.util.Collection;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import com.example.demo.model.Customer;

import org.springframework.data.domain.Pageable;

public interface ICustomerService {

    public Collection<Customer> findAll(Optional<Pageable> pageable);

    public Customer findById(Long id) throws EntityNotFoundException;

    public boolean exists(Long id) throws EntityNotFoundException;

    public Customer save(Customer customer);

    public boolean delete(Long id) throws EntityNotFoundException;
}
